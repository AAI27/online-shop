# tutorial_online_shop

## Project setup
```
npm install
```

### To see json server
```
json-server --watch db.json
```

### Compiles and hot-reloads for development
```
npm run serve
```


